/*
 * Copyright (C) 2015-2016 Ozan Egitmen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.egitmen.shutdownscheduler;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FrmMain extends javax.swing.JFrame {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(FrmMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        FrmMain gui = new FrmMain();
        gui.setVisible(true);
    }

    Boolean stop = false;
    java.util.prefs.Preferences prefs = java.util.prefs.Preferences.userRoot().node(this.getClass().getName());
    TrayIcon trayIcon;

    public FrmMain() {
        initComponents();
        getContentPane().setBackground(Color.WHITE);
        sldHours.setValue(prefs.getInt("Hours", 0));
        sldMinutes.setValue(prefs.getInt("Minutes", 0));
        cbMinimizeToTray.setSelected(prefs.getBoolean("MinimizeToTray", false));
        addWindowStateListener((java.awt.event.WindowEvent we) -> {
            if (cbMinimizeToTray.isSelected() && (we.getNewState() & Frame.ICONIFIED) == Frame.ICONIFIED) {
                setVisible(false);
                SystemTray tray = SystemTray.getSystemTray();
                ActionListener exitListener = (ActionEvent e) -> {
                    System.exit(0);
                };
                PopupMenu popup = new PopupMenu();
                ActionListener stopListener = (ActionEvent e) -> {
                    stop = true;
                    btnStart.setEnabled(true);
                    btnStop.setEnabled(false);
                    popup.remove(popup.getItem(0));
                };
                if (btnStop.isEnabled()) {
                    MenuItem stopItem = new MenuItem("Stop");
                    stopItem.addActionListener(stopListener);
                    popup.add(stopItem);
                }
                MenuItem exitItem = new MenuItem("Exit");
                exitItem.addActionListener(exitListener);
                popup.add(exitItem);
                trayIcon = new TrayIcon(getIconImage(), "Shutdown Scheduler", popup);
                ActionListener actionListener = (ActionEvent e) -> {
                    setVisible(true);
                    setState(0);
                    tray.remove(trayIcon);
                };
                trayIcon.setImageAutoSize(true);
                trayIcon.addActionListener(actionListener);
                try {
                    tray.add(trayIcon);
                } catch (AWTException ex) {
                    Logger.getLogger(FrmMain.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    private String msToClock(Integer ms) {
        String clock = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(ms), TimeUnit.MILLISECONDS.toMinutes(ms) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(ms)), TimeUnit.MILLISECONDS.toSeconds(ms) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(ms)));
        return clock;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblHours = new javax.swing.JLabel();
        lblMinutes = new javax.swing.JLabel();
        sldHours = new javax.swing.JSlider();
        tfHours = new javax.swing.JTextField();
        sldMinutes = new javax.swing.JSlider();
        tfMinutes = new javax.swing.JTextField();
        btnStart = new javax.swing.JButton();
        btnStop = new javax.swing.JButton();
        lblShutdown = new javax.swing.JLabel();
        tfShutdown = new javax.swing.JTextField();
        cbMinimizeToTray = new javax.swing.JCheckBox();
        lblBitbucket = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Shutdown Scheduler");
        setIconImage(new ImageIcon(getClass().getResource("/com/egitmen/shutdownscheduler/ImgClock.png")).getImage());
        setResizable(false);

        lblHours.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 18)); // NOI18N
        lblHours.setText("Hours:");

        lblMinutes.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 18)); // NOI18N
        lblMinutes.setText("Minutes:");

        sldHours.setMajorTickSpacing(1);
        sldHours.setMaximum(8);
        sldHours.setPaintTicks(true);
        sldHours.setValue(0);
        sldHours.setFocusable(false);
        sldHours.setOpaque(false);
        sldHours.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sldHoursStateChanged(evt);
            }
        });

        tfHours.setEditable(false);
        tfHours.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 18)); // NOI18N
        tfHours.setText("0");
        tfHours.setBorder(null);
        tfHours.setFocusable(false);
        tfHours.setOpaque(false);

        sldMinutes.setMajorTickSpacing(1);
        sldMinutes.setMaximum(11);
        sldMinutes.setPaintTicks(true);
        sldMinutes.setSnapToTicks(true);
        sldMinutes.setValue(0);
        sldMinutes.setFocusable(false);
        sldMinutes.setOpaque(false);
        sldMinutes.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sldMinutesStateChanged(evt);
            }
        });

        tfMinutes.setEditable(false);
        tfMinutes.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 18)); // NOI18N
        tfMinutes.setText("0");
        tfMinutes.setBorder(null);
        tfMinutes.setFocusable(false);
        tfMinutes.setOpaque(false);

        btnStart.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 15)); // NOI18N
        btnStart.setText("START");
        btnStart.setFocusable(false);
        btnStart.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnStartMouseClicked(evt);
            }
        });

        btnStop.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 15)); // NOI18N
        btnStop.setText("STOP");
        btnStop.setEnabled(false);
        btnStop.setFocusable(false);
        btnStop.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnStopMouseClicked(evt);
            }
        });

        lblShutdown.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 18)); // NOI18N
        lblShutdown.setText("Counter:");

        tfShutdown.setEditable(false);
        tfShutdown.setFont(new java.awt.Font("Courier New", 0, 19)); // NOI18N
        tfShutdown.setFocusable(false);
        tfShutdown.setOpaque(false);

        cbMinimizeToTray.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 15)); // NOI18N
        cbMinimizeToTray.setText("Minimize to system tray?");
        cbMinimizeToTray.setFocusable(false);
        cbMinimizeToTray.setOpaque(false);
        cbMinimizeToTray.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                cbMinimizeToTrayStateChanged(evt);
            }
        });

        lblBitbucket.setFont(new java.awt.Font("Microsoft YaHei UI Light", 0, 18)); // NOI18N
        lblBitbucket.setText("<html><u>Bitbucket");
        lblBitbucket.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblBitbucketMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblMinutes)
                            .addComponent(lblHours))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(sldMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sldHours, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tfHours, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbMinimizeToTray)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnStart, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnStop, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(lblShutdown)))
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(tfShutdown, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblBitbucket, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(26, 26, 26))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(sldHours, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblHours)
                    .addComponent(tfHours, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMinutes)
                    .addComponent(tfMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sldMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnStart, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnStop, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblShutdown, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfShutdown, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBitbucket, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbMinimizeToTray, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(9, 9, 9))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void sldHoursStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sldHoursStateChanged
        Integer value = sldHours.getValue();
        tfHours.setText(value.toString());
        prefs.putInt("Hours", value);
    }//GEN-LAST:event_sldHoursStateChanged

    private void sldMinutesStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sldMinutesStateChanged
        Integer value = sldMinutes.getValue();
        tfMinutes.setText(String.valueOf(value * 5));
        prefs.putInt("Minutes", value);
    }//GEN-LAST:event_sldMinutesStateChanged

    private void btnStartMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStartMouseClicked
        if (!SwingUtilities.isLeftMouseButton(evt)
                || !btnStart.isEnabled()
                || (sldHours.getValue() == 0 && sldMinutes.getValue() == 0)) {
            return;
        }
        btnStop.setEnabled(true);
        btnStart.setEnabled(false);
        sldHours.setEnabled(false);
        sldMinutes.setEnabled(false);
        Integer hours = Integer.parseInt(tfHours.getText());
        Integer minutes = Integer.parseInt(tfMinutes.getText());
        Integer ms = hours * 3600000 + minutes * 60000;
        new java.util.Timer().schedule(new java.util.TimerTask() {
            int time = ms + 900;

            @Override
            public void run() {
                if (stop) {
                    tfShutdown.setText("");
                    stop = false;
                    this.cancel();
                    return;
                }
                String clock = msToClock(time);
                if (time != 0) {
                    tfShutdown.setText(clock);
                    time -= 100;
                    if (time == 60000 && !isVisible()) {
                        trayIcon.displayMessage("Shutdown Scheduler", "System shutdown in 60 seconds!", TrayIcon.MessageType.WARNING);
                    }
                } else {
                    try {
                        Runtime.getRuntime().exec("shutdown -s");
                    } catch (IOException ex) {
                        Logger.getLogger(FrmMain.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.exit(0);
                }
            }
        }, 0, 100);

    }//GEN-LAST:event_btnStartMouseClicked

    private void btnStopMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStopMouseClicked
        if (SwingUtilities.isLeftMouseButton(evt) && btnStop.isEnabled()) {
            stop = true;
            btnStop.setEnabled(false);
            btnStart.setEnabled(true);
            sldHours.setEnabled(true);
            sldMinutes.setEnabled(true);
        }
    }//GEN-LAST:event_btnStopMouseClicked

    private void lblBitbucketMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblBitbucketMouseClicked
        if (SwingUtilities.isLeftMouseButton(evt)) {
            try {
                java.awt.Desktop.getDesktop().browse(new java.net.URI("https://bitbucket.org/654wak654/shutdown-scheduler"));
            } catch (java.net.URISyntaxException | IOException ex) {
                Logger.getLogger(FrmMain.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_lblBitbucketMouseClicked

    private void cbMinimizeToTrayStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_cbMinimizeToTrayStateChanged
        prefs.putBoolean("MinimizeToTray", cbMinimizeToTray.isSelected());
    }//GEN-LAST:event_cbMinimizeToTrayStateChanged
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnStart;
    private javax.swing.JButton btnStop;
    private javax.swing.JCheckBox cbMinimizeToTray;
    private javax.swing.JLabel lblBitbucket;
    private javax.swing.JLabel lblHours;
    private javax.swing.JLabel lblMinutes;
    private javax.swing.JLabel lblShutdown;
    private javax.swing.JSlider sldHours;
    private javax.swing.JSlider sldMinutes;
    private javax.swing.JTextField tfHours;
    private javax.swing.JTextField tfMinutes;
    private javax.swing.JTextField tfShutdown;
    // End of variables declaration//GEN-END:variables
}
